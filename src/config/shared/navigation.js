import Root from '../../screen/BottomTab/index'
import Logar from '../../screen/Authentication/login'
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

const container = createAppContainer(createSwitchNavigator(
    {
        Login: Logar,
        App: Root

    },
    {
        initialRouteName: 'App',
    }
));

export default AppNavigation = (authenticated) => container