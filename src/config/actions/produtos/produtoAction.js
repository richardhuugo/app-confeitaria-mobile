import axios from 'axios';
import { listarProdutosUrl } from '../../const'
export const listarProdutos = () => {
    return dispatch => {

        axios.get(listarProdutosUrl, {
            params: {
                empresa_id: 'NWM0NWNlMzcwOTc2Y2YxYTA4YzYxYzIy'
            }
        })
            .then(function (response) {
                dispatch({
                    type: 'LISTAR_PRODUTOS',
                    payload: response.data.produtos
                })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })



    }
}