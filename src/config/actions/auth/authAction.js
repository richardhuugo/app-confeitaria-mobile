import { AsyncStorage } from 'react-native';
const authSucess = (token) => {
    return dispatch => {
        AsyncStorage.multiSet([['token', token], ['loggedin', '1']])
        dispatch({ type: 'LOGIN_SUCESS', payload: null })

    }
}

const checkLogin = () => {


    return async dispatch => {
        const isLoggin = await AsyncStorage.getItem("authenticated").catch(e => console.log(e))

        if (isLoggin) {
            dispatch({ type: 'LOGIN_SUCESS', payload: null })
        }
        dispatch({ type: 'APP_LOADED', payload: null })
    }



}

export { authSucess, checkLogin }