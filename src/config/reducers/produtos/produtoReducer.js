const INITIAL_STATE = { produtos: [] };
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'LISTAR_PRODUTOS':
            return { ...state, produtos: action.payload }
        default:
            return state;
    }
};