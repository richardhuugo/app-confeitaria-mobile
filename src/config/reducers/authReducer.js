const INITIAL_STATE = { app_started: false, authenticated: false };

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'LOGIN_SUCESS':
            return { ...state, authenticated: true }
        case 'LOGGOUT':
            return { ...state, authenticated: false }
        case 'APP_LOADED':
            return { ...state, app_started: true }
        default:
            return state
    }
}