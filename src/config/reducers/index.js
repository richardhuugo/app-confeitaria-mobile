import { combineReducers } from 'redux';
import AppReducer from './appReducer';
import authStateReducer from './authReducer'
import produtoReducer from './produtos/produtoReducer';

export default combineReducers({
    app: AppReducer,
    authState: authStateReducer,
    produtoService: produtoReducer
});