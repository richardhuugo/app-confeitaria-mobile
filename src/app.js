import React, { PureComponent } from 'react';
import { Text, View, ActivityIndicator, AsyncStorage } from 'react-native';
import AppNavigation from './config/shared/navigation'
import { connect } from 'react-redux'
import { checkLogin } from './config/actions/auth/authAction';


class AppRoot extends PureComponent {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        this.props.checkLogin();
    }
    render() {

        const { app_started, authenticated } = this.props.authState
        return app_started ? this._renderAppRoot(authenticated) : this._renderSplash();

    }

    _renderAppRoot(authenticated) {
        const CreateRoot = AppNavigation(authenticated);
        return <CreateRoot />
    }

    _renderSplash() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', backgroundColor: '#F5FCFF' }}>
                <ActivityIndicator size='large' />
                {console.disableYellowBox = true}
                <Text children="Loading... " />
            </View>
        )
    }
}
const mapStateToProps = (state, onwProps) => ({
    authState: state.authState
})

export default connect(mapStateToProps, { checkLogin })(AppRoot)