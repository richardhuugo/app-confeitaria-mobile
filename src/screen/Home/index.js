import { createAppContainer, createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import ProdutosScreen from './produtos'
import DetalhesScreen from './detalhes'

export default (createStackNavigator(
    {
        Produtos: {
            screen: ProdutosScreen,
        },
        Detalhes: {
            screen: DetalhesScreen,
        },
    },
    {
        initialRouteName: 'Produtos',
    })
);