import React from 'react'
import { View, Text, Button } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json

export default class Detalhes extends React.Component {
    static navigationOptions = {
        headerTitle: 'Detalhes',

    };


    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details produtos</Text>
                <Button
                    title="Go to Produtos"
                    onPress={() => {
                        this.props.navigation.dispatch(StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'Produtos' })
                            ],
                        }))
                    }}
                />
            </View>
        )
    }
}