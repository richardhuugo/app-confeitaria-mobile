import React from 'react'
import { View, Text, Button, Image, ListView, ImageBackground, TouchableHighlight, FlatList, SafeAreaView, StyleSheet } from 'react-native'
import _ from 'lodash';
import { StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { listarProdutos } from '../../config/actions/produtos/produtoAction'
import { connect } from 'react-redux'
import Card from './productCard'
import { ScrollView } from 'react-native-gesture-handler';
//https://medium.com/@oieduardorabelo/react-native-criando-grids-com-flatlist-b4eb64e7dcd5
class Produtos extends React.Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props)

    }
    componentDidMount() {
        this.props.listarProdutos();
    }


    _loadProdutos(produto) {
        return (produto.map((produto) => {

            return (<View style={{ width: 150, height: 150, margin: 5 }} >
                <Card key={produto._id} title={produto.nome} descricao={produto.descricao} imagem={produto.imagem} categoria={produto.categoria} />
            </View>)
        }))
    }
    _createRows(row, colum) {
        const rows = Math.floor(row.length / colum);
        let lastRowElements = row.length - rows * colum;
        while (lastRowElements !== colum) { // [C]
            row.push({
                categoria: [],
                descricao: '',
                empresa: [],
                imagem: [],
                nome: '',
                peso: '',
                tp_peso: [],
                empty: true
            })

            lastRowElements += 1; // [E]
        }
        return row;
    }
    render() {
        const { produtos } = this.props
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <FlatList
                    data={this._createRows(produtos, 2)}
                    keyExtractor={item => item.id}
                    numColumns={2} // Número de colunas
                    renderItem={({ item }) => {
                        if (item.empty) {
                            return <View style={[styles.item, styles.itemEmpty]} />;
                        }
                        return (
                            <View style={styles.item}>


                                <Card key={item._id} title={item.nome} item={item} navigation={this.props.navigation} descricao={item.descricao} imagem={item.imagem} categoria={item.categoria} />


                            </View>
                        );
                    }}


                />
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    item: {
        alignItems: "center",
        flexBasis: 0,
        height: 'auto',
        flexGrow: 1,
        margin: 12,

    },
    itemEmpty: {
        backgroundColor: "transparent"
    },
    text: {
        color: "#333333"
    }
});


/**
      return (<TouchableOpacity key={produto._id} onPress={() => {
          this.props.navigation.navigate('Detalhes', {
              detalhesObjecto: produto
          });

      }} >
          <Card key={produto._id} title={produto.nome} descricao={produto.descricao} imagem={produto.imagem} categoria={produto.categoria} />
      </TouchableOpacity>) */
/** <ScrollView>
            <View style={{ flex: 1,     flexDirection:'row' ,     }} >
               
                    {console.log(produtos)}
                    { this._loadProdutos(produtos)  }
                


            </View>
            </ScrollView>  <Button
                    title="Go to Details"
                    onPress={() => {
                        this.props.navigation.dispatch(StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'Detalhes' })
                            ],
                        }))
                    }}
                /> */
const mapStateToProps = state => ({
    produtos: state.produtoService.produtos
})
export default connect(mapStateToProps, { listarProdutos })(Produtos)