import React from 'react'
import { View, Text, Image, ImageBackground, TouchableHighlight, Dimensions, ScrollView, ActivityIndicator } from 'react-native'
import ReadMore from 'react-native-read-more-text';
const { width, height } = Dimensions.get('window');
export default class productCard extends React.Component {
    componentDidMount() {
        let scrollValue = 0;

    }

    _carregarCategoria = (categoria) => {
        return (
            categoria.map((category) => {
                return (
                    <Text key={category._id} style={{ color: 'gray', marginTop: 0, fontSize: 9 }}>{category.tipo} </Text>)
            })
        )
    }
    _carregarImagem = (imagem) => {
        return (imagem.map((img, index) => {
            return (<ImageBackground
                key={index}
                indicatorStyle={<ActivityIndicator />}
                source={{ uri: img }}
                imageStyle={{ borderRadius: 5 }}
                style={{ margin: 5, width: 145 }} />)
        }))
    }
    render() {
        const { title, categoria, descricao, imagem } = this.props
        return (
            <View style={{

                backgroundColor: 'white',
                borderRadius: 10,
                borderColor: 'black',
                padding: 2,
                height: 240, elevation: 0.9,
                shadowRadius: 1,
                shadowOffset: {
                    height: 10,
                    width: 0
                }, flexDirection: 'column',
            }} >
                <View style={{ flexDirection: 'column' }} >
                    <ScrollView

                        indicatorStyle='white'

                        ref={(scrollView) => { _scrollView = scrollView; }}
                        horizontal={true}
                        pagingEnabled={true}
                        decelerationRate={0}
                        snapToInterval={width - 60}
                        snapToAlignment={"center"}
                        contentInset={{
                            top: 0,
                            left: 5,
                            bottom: 0,
                            right: 5,
                        }}

                    >
                        {this._carregarImagem(imagem)}

                    </ScrollView>


                    <View style={{ marginTop: 10 }}  >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                            <Text
                                style={{ color: 'black', fontSize: 14 }}
                            >{title}</Text>
                            <Text
                                style={{ color: 'black', fontSize: 8 }}
                            >Price</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            {this._carregarCategoria(categoria)}
                        </View>

                        <ReadMore
                            numberOfLines={2}
                            onReady={this._handleTextReady}>
                            <Text
                                style={{ color: 'black', marginTop: 5, fontSize: 10 }}
                            >
                                {descricao}
                            </Text>
                        </ReadMore>
                        <TouchableHighlight key={this.props.key} onPress={() => {
                            this.props.navigation.navigate('Detalhes', {
                                detalhesObjecto: this.props.item
                            });

                        }} >
                            <View
                                style={{
                                    alignItems: "center",


                                    flexDirection: 'row', backgroundColor: 'blue'
                                }}
                            >
                                <Text
                                    style={{ color: 'white' }}
                                >Detalhes</Text>
                            </View>
                        </TouchableHighlight>
                    </View>

                </View>



            </View >
        )
    }

    //rgba(0, 0, 0, 0.66)

}