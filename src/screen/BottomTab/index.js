import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import CarrinhoScreen from './carrinho'
import ConfiguracaoScreen from './configuracao'
import HomeScreen from './home'

const TabNavigator = createMaterialBottomTabNavigator({
    Home: {
        screen: HomeScreen,
    },
    Carrinho: {
        screen: CarrinhoScreen
    },
    Configuracao: {
        screen: ConfiguracaoScreen
    }

}, {
        initialRouteName: 'Home',
        activeTintColor: 'black',
        inactiveColor: 'gray',
        barStyle: { backgroundColor: 'white' },
    });

//export default createAppContainer(TabNavigator);
export default (TabNavigator);

