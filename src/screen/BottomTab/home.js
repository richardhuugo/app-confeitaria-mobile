import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
import HomePage from '../Home/index'

export default class HomeScreen extends Component {
    static router = HomePage.router;
    static navigationOptions = {
        title: 'Home',
    };
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <HomePage navigation={this.props.navigation} />
                {/** adicionar outras coisas */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
