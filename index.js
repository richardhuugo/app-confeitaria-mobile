/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React from 'react'
import { AppRegistry } from 'react-native';
import { Provider, } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import App from './src/app';
import { name as appName } from './app.json';
import Reducers from './src/config/reducers/index'

const store = createStore(Reducers, applyMiddleware(thunk)),
    appRootComponent = () => (
        <Provider store={store}>
            <App />
        </Provider>
    )

AppRegistry.registerComponent(appName, () => appRootComponent);
